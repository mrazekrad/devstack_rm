<?php
ob_start();

require_once "temp/header.php";

$buffer=ob_get_contents();
ob_end_clean();

$buffer=str_replace("%TITLE%","Titulek",$buffer);
$buffer=str_replace("%DESCRIPTION%","Description.",$buffer);
echo $buffer;
?>

<body class="">

    <header>
        <?php require_once "temp/nav.php";?>
    </header>

    <main>
        <section class="container-fluid" id="">
            <div class="container" id="">
                <div class="row">
                    Tady bude obsah
                </div><!--row-->
            </div><!--container-->
        </section><!--container-fluid-->
    </main>
  
    <?php require_once "temp/footer.php";?>