<!DOCTYPE html>
<html lang="cs">
<head>
    <!-- Meta, Title -->
    <meta charset="UTF-8">
    <meta name="author" content="Bc. Radek Mrázek" />
    <meta name="description" content="%DESCRIPTION%" />
    <meta name="keywords" content="%KEYWORDS%">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>%TITLE%</title>

    <!--favicon-->
    <link rel="icon" type="image/png" href="img/favicon/favicon.png"/>

    <!--style-->
    <link rel="stylesheet" type="text/css" href="css/style.css?v=<?php echo time(); ?>">

    <!--bootstrap grid-->
    <link rel="stylesheet" type="text/css" href="css/libs/bootstrap-grid.min.css">


    <!--script-->
    <script src="js/main.js"></script>

</head>
